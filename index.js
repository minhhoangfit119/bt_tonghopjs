// bài 1
function bai1() {

  const max = 100;

  let content = `<center>`;

  for (let i = 1; i <= max; i++) {
    content += i;
    if (i % 10 === 0) {
      content += `<center/> <center>`;
    }
    document.getElementById('txtIn').innerHTML = `${content}</center>`;
  }
}

// bài 2
let arr = [];
document.getElementById('themBai2').onclick = function(){
  var input_bai2 = document.getElementById('input_bai2')

  if(input_bai2.value !== ''){
    arr.push(input_bai2.value)
  }

  document.getElementById('txtBai2').innerHTML = arr.join();
}

document.getElementById('thembai2').onclick = function(){
   var arrSoNguyen = arr.filter(num =>{
       if(num < 2){
        return false;
       }
       
       else if(num == 2){
        return true;
       }

       else{
        for(var i = 2; i < num; i++){
          if(num % i === 0){
            return false 
          }
        }
        return true;
       }
   })
   document.getElementById('txtbai2').innerHTML = arrSoNguyen.join();
}

// bài 3
function bai3(){
   var bai3 = document.getElementById('input_bai3').value * 1;
   
   var tong = 0;

   if(bai3 === ''){
    return;
   }

   else{
    for (var i = 2; i <= bai3; i++){
      tong += i
    }
   }
   document.getElementById('txtTinh').innerHTML = tong + 2*bai3;
}

// bài 4
function bai4(){
  var arr = [];

  var bai4 = document.getElementById('input_bai4').value * 1;

  for(var i = 1; i <= bai4; i++){
    if(bai4 % i === 0){
      arr.push(i)
    }
  }

  document.getElementById('txtBai4').innerHTML = `Ước của ${bai4}: ${arr.join()}`
}

// bài 5
function bai5(){
  var input_bai5 = document.getElementById('input_bai5').value * 1;
    if(input_bai5 === ''){
      return;
    }

    else{
      document.getElementById('txtBai5').innerHTML = input_bai5.toString().split('').reverse().join('');
    }
}

// bài 6
function bai6(){
  var input_bai6 = document.getElementById('input_bai6').value * 1;
  
  var arr = [];
  var count = 0;

  for (var i = 0; i <= input_bai6; i++){
     if(count + 1 < input_bai6){
      count += i;
      arr.push(i);
     }
  }
  console.log(arr[-1]);
  document.getElementById('txtBai6').innerHTML = `X = ${arr[arr.length-1]}`;
}
// bài 7
function bai7(){
  var input_bai7 = document.getElementById('input_bai7').value * 1;

  var content = ``;

  for(var i = 1; i <= 10; i++){
    content += `${input_bai7} * ${i} = ${input_bai7 * i} </br>`;
  }
  document.getElementById('txtBai7').innerHTML = content;
}

// bài 8
function bai8(){
  var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
"AS", "7H", "9K", "10D"]
  
  var count = 0;
  var players = [ [], [], [], [] ];

  cards.forEach(arr =>{
    if(count > 3){
      count = 0;
    }
    players[count].push(arr);
    count++;
  })

  document.getElementById('user1').innerHTML = `Người chơi 1: ${players[0]}`;
  document.getElementById('user2').innerHTML = `Người chơi 2: ${players[1]}`;
  document.getElementById('user3').innerHTML = `Người chơi 3: ${players[2]}`;
  document.getElementById('user4').innerHTML = `Người chơi 4: ${players[3]}`;
}

//bài 9
function bai9(){
  var tongSoChan = document.getElementById('input_2').value * 1;
  var tongSoGaVaCho = document.getElementById('input_1').value * 1;

  if(tongSoChan > 0 && tongSoGaVaCho > 0 && tongSoChan > tongSoGaVaCho){
    var x = (tongSoChan -(tongSoGaVaCho * 4)) / (-2)
    var y = tongSoGaVaCho - x
    document.getElementById('txtBai9').innerHTML = `Tổng số con gà: ${x}
                                                    Tổng số con chó: ${y}`
  }
}

//bài 10
function themBai10(){
  var input_hour = document.getElementById('input_hour').value * 1;
  var input_minute = document.getElementById('input_minute').value * 1;

  var toado = 360;
  var hour = 12;
  var minute = 60;

  if(input_hour === '' || input_minute === '' || input_minute > 60 || input_minute < 0 || input_hour > 12 || input_hour <0){
    alert('Xin mời nhập lại')
  }
  else{
    let gocGio = toado / hour * input_hour;
    console.log("🚀 ~ file: scripts.js:161 ~ themBai10 ~ gocGio", gocGio)
    let goPhut = toado / minute * input_minute;
    console.log("🚀 ~ file: scripts.js:163 ~ themBai10 ~ goPhut", goPhut)
    document.getElementById('txtBai10').innerHTML = `Góc lệch giữa kim giờ và kim phút: ${gocGio - goPhut} độ`
  }

}
